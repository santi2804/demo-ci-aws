import sys, json, argparse

parser = argparse.ArgumentParser('Replaces image in the task definition')
parser.add_argument('image_uri', metavar='I', type=str, nargs='+',
                   help='The new image URI')

fd = open(sys.stdin)
json_data = fd.read()
print(json_data)

args = parser.parse_args()
definition = json.load(sys.stdin)['taskDefinition']['containerDefinitions']
definition[0]['image'] = args.image_uri[0]

definition[0]['cpu'] =256
definition[0]['memory'] =512

print(json.dumps(definition))