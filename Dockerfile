FROM maven:3.8.2-openjdk-17-slim AS build
#FROM eclipse-temurin:17-jdk-alpine

# PROJECT SETUP

WORKDIR /app

COPY . /app/
RUN mvn -f /app/pom.xml clean package
#RUN mvn clean install

#CMD mvn spring-boot:run

## RUN project
FROM openjdk:17-alpine
COPY --from=build /app/target/demo-0.0.1-SNAPSHOT.jar /app/demo.jar
EXPOSE 8080
ENTRYPOINT [ "java", "-jar", "/app/demo.jar" ]